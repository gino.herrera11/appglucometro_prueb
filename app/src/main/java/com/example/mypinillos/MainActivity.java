package com.example.mypinillos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT=0;
    private static final int REQUEST_DISCOVER_BT = 1;
    private TextView statusb, resultado, resultado2;
    private ImageView imagenblue;
    private Button encender, apagar, reconocible, mostrar;

    BluetoothAdapter mBluetoothAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        statusb = findViewById(R.id.statusBluetootv);
        resultado = findViewById(R.id.paiRedTv);
        resultado2 = findViewById(R.id.ObtenerDatos);
        imagenblue = findViewById(R.id.bluetoothIv);
        encender = findViewById(R.id.OnBtn);
        apagar = findViewById(R.id.OfBtn);
        reconocible = findViewById(R.id.ReconocerBtn);
        mostrar = findViewById(R.id.DispoEmparejado);
        //ADAPTER

        mBluetoothAdapter  = BluetoothAdapter.getDefaultAdapter();
        //revisar el el bluetooth esta disponible o no

        if (mBluetoothAdapter == null)
        {
            statusb.setText("Bluettoth no esta disponible");
        }else{
            statusb.setText("Bluetooth");
        }

        //mostrar imagenes
        if(mBluetoothAdapter.isEnabled())
        {
            imagenblue.setImageResource(R.drawable.ic_action_on);
        }else{
            imagenblue.setImageResource(R.drawable.ic_action_off);
        }
        //boton encender
        encender.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!mBluetoothAdapter.isEnabled())
                {
                    mostrartoast("bluetooth  esta encendiendo");
                    imagenblue.setImageResource(R.drawable.ic_action_on);


                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent,REQUEST_ENABLE_BT);
                }else{
                    mostrartoast("bluetooth  esta encendido");

                }
            }
        });

        //boton discover

        reconocible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBluetoothAdapter.isDiscovering())
                {
                    mostrartoast("haciendo que su dispositivo sea reconocible");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult( intent, REQUEST_DISCOVER_BT);
                }
            }
        });

        //boton apagar

        apagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBluetoothAdapter.isEnabled())
                {
                    mBluetoothAdapter.disable();
                    mostrartoast("Apagando bluetooth");

                    imagenblue.setImageResource(R.drawable.ic_action_off);
                }else{
                    mostrartoast("El bluetooth ya esta apagado");
                }
            }
        });

        //boton MOSTRTAR DISPOSITIVOS EMPAREJADOS

        mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBluetoothAdapter.isEnabled())
                {
                    resultado.setText("DISPOSITIVOS EMPAREJADOS");
                    Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
                    for (BluetoothDevice device:devices){
                        resultado.append("\nDispositivo:" + device.getName() + "," + device);

                    }
                }else{
                    //si el bluetooth no esta apgado no se puede obtener los dispositivos
                    mostrartoast("Encienda el bluetooth para ver los dispositivos");

                }
            }
        });



        resultado2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        })

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    switch ((requestCode)) {
        case  REQUEST_ENABLE_BT:

            if (resultCode == RESULT_OK)
            {
                //BLUETOOTH ON
                imagenblue.setImageResource(R.drawable.ic_action_on);
                mostrartoast("El bluetooth esta encendido");
            }else{
                mostrartoast("No se realizo la accion");
            }
            break;
    }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void mostrartoast (String msg) { Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();}



    //obtener datos




}
